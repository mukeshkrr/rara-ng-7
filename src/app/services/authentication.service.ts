import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  SERVER_URL = "http://api.rara.delivery:8081/";

  login(email: string, password: string) {



        const options = {
               headers: new HttpHeaders({ 'Authorization': 'Bearer '+localStorage.getItem("token") })
        };


        return this.http.post<any>(this.SERVER_URL+'login/getLoginUser?email='+email+'&password='+password, { email, password }, options)
    }

    
    register( name: string, email: string, password: string) {

        const data = {
            "name": name,
            "login": email,
            "password": password,
            "roles": [{
                "id": 1
            }]
        }

        const myheader = new HttpHeaders().set('Content-Type','application/json');
        console.log(myheader);
        return this.http.post<any>(this.SERVER_URL+'/signup', data, {headers:myheader})
        
    }

    logout() {
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        localStorage.removeItem('username');
    }


    generateToken(email: string, password: string){
        
        let formData: FormData = new FormData(); 
        formData.append("grant_type", "password");
        formData.append("username", email);
        formData.append("password", password);
        formData.append("scope", "read write");
        formData.append("client_secret", "123456");
        formData.append("client_id", "clientapp");

        /*const pairs = Array.from(formData.entries());
        for (let pair of pairs) {
            console.log(pair);
        }*/

        let authdata = btoa("clientapp" + ':' + "123456");

        let headers = new Headers();
        headers.append('Authorisation', 'Basic '+authdata);

        const options = {
               headers: new HttpHeaders({ 'Authorization': 'Basic '+authdata })
        };

        return this.http.post<any>(this.SERVER_URL+'oauth/token', formData, options);
        
       
    }








}
