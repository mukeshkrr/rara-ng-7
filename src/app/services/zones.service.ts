import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ZonesService {

  constructor(private http: HttpClient) { }

  SERVER_URL = "http://api.rara.delivery:8081/";


  getAllDistricts(){

  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"zone/districtList", {headers:myheader});

  }

  getUsersByRole(id: number){

  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"zone/userbyrole/"+id, {headers:myheader});

  }

  getZonesByBusiness(uid: number){

  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"zone/zonelist/"+uid, {headers:myheader});

  }

  zoneWithBookingList(){
  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"zone/zone-booking-list", {headers:myheader});
  }

  getBookingZoneListByBusiness(uid: number){
  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"zone/zone-booking-list/"+uid, {headers:myheader});
  }

  updateZone(zone: Object){
    
        const myheader = new HttpHeaders().set('Content-Type','application/json');
        return this.http.post<any>(this.SERVER_URL+'zone/updatezone',zone, {headers:myheader});

  }

  getPostalCodeListInAZone(zoneId: number){

        const myheader = new HttpHeaders().set('Content-Type','application/json');

        return this.http.get(this.SERVER_URL+"zone/getzone/"+zoneId, {headers:myheader});


  }

  deleteZipZoneMapping(zoneId: number, zoneZipMapId: number){
    const myheader = new HttpHeaders().set('Content-Type','application/json');
        return this.http.get(this.SERVER_URL+"zone/delete-zone-zip-map/"+zoneId+"/"+zoneZipMapId, {headers:myheader});
  }

  deleteAZone(zoneId: number){
    const myheader = new HttpHeaders().set('Content-Type','application/json');
        return this.http.get(this.SERVER_URL+"zone/deletezone/"+zoneId, {headers:myheader});
  }


  getSubDistrict(district: string){
    const myheader = new HttpHeaders().set('Content-Type','application/json');
        return this.http.get(this.SERVER_URL+"zone/subdistrictList/"+district, {headers:myheader});
  }


















}
