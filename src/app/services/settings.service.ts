import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private http: HttpClient) { }

  SERVER_URL = "http://api.rara.delivery:8081/";

  saveApi(keyTitle: string, metaKey: string, matrixapikey: string){

  	const data = {
            "keyTitle": keyTitle,
            "metaKey": metaKey,
            "matrixapikey": matrixapikey,
            "haltTime": "0"
        }

        const myheader = new HttpHeaders().set('Content-Type','application/json');
        console.log(myheader);
        return this.http.post<any>(this.SERVER_URL+'settings/add-api', data, {headers:myheader})

  }

  getApisList(){

  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"settings/list", {headers:myheader});

  }

  deleteASetting(id: number){

  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"settings/delete-api/"+id, {headers:myheader});

  }

  generateLzWebHookToken(){

  	const myheader = new HttpHeaders().set('Content-Type','application/json');

  	return this.http.get(this.SERVER_URL+"settings/generate-webhook-token", {headers:myheader});

  }



}
