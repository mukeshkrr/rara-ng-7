import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import * as $ from 'jquery';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  auth: Object;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {

  	$(document).ready(function(){
  		$("body").addClass("hold-transition login-page");
  	});

  	this.loginForm = this.formBuilder.group({
          email: ['', Validators.required],
          password: ['', Validators.required]
      });

    this.authenticationService.logout();  

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'; 

  }


  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;


    this.authenticationService.generateToken(this.f.email.value, this.f.password.value).subscribe((auth:any) => {
      this.auth = auth;

      if(auth.access_token.length > 5){

        localStorage.setItem("username",this.f.email.value);
        localStorage.setItem("token",auth.access_token);

        this.authenticationService.login(this.f.email.value, this.f.password.value).subscribe(
        response => {
          this.loading = false;
          if(response.status != 'Success'){
                this.error = response.message;
                
              }
          if(response.status == 'Success'){
                
                localStorage.setItem('user', response.data);

                if (response.data.companyName == null && response.data.companyAddress == null && response.data.website == null && response.data.theyDo == null && response.data.needDeliver == null && response.data.pickUpLocation == null) {
                    localStorage.setItem('initialApp', 'true');
                    
                } else if (response.data.roles[0].id == 2) {
                    this.router.navigate(['/live-orders.html']);
                } else {
                    this.router.navigate(['/live-orders.html']);
                }

              }

              this.loading = false;

        },
        error => {
            this.loading = false;
        }

        );

      }





    });  



    
}








}
