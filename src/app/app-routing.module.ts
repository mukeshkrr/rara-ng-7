import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LiveOrdersComponent } from './components/live-orders/live-orders.component';
import { CreateZoneComponent } from './components/create-zone/create-zone.component';

import { HomeComponent } from './components/home/home.component';

import { SettingsComponent } from './components/settings/settings.component';

const routes: Routes = [
	  { path: 'live-orders.html', component: LiveOrdersComponent, canActivate: [AuthGuard] },
	  { path: 'settings.html', component: SettingsComponent, canActivate: [AuthGuard] },	
	  { path: 'create-zone.html', component: CreateZoneComponent, canActivate: [AuthGuard] },
	  { path: '', component: HomeComponent },	
	  { path: 'login.html', component: LoginComponent },
	  { path: 'register.html', component: RegisterComponent }, 
	   
	    // otherwise redirect to home
	  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
