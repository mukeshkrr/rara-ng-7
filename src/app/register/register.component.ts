import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  auth: Object;
  errmsg: string;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {


  	$(document).ready(function(){
  		$("body").addClass("hold-transition register-page");
  	});

  	this.registerForm = this.formBuilder.group({
        name: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', Validators.required],
        repassword: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {

  	this.submitted = true;
  	if (this.registerForm.invalid) {
        return;
    }
    this.loading = true;

    if (this.f.password.value != this.f.repassword.value) {
            this.error = "Password does not match.";
           
            return;
        }

    this.authenticationService.register(this.f.name.value, this.f.email.value, this.f.password.value)
    .pipe(first())
    .subscribe(
    	response => {


    		if (response.status == "Error") {
                this.errmsg = "This user already exist";
                alert(this.errmsg);
                
            } else {
                
                alert("we are verifying you and will be in touch soon.");

            }

            this.loading = false;

    	},
    	error => {
    		alert("Something went wrong");
    		this.loading = false;
    	}
    );    







  }









}
