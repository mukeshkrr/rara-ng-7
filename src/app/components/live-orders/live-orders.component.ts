import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-live-orders',
  templateUrl: './live-orders.component.html',
  styleUrls: ['./live-orders.component.css']
})
export class LiveOrdersComponent implements OnInit {

  title = 'LIVE ORDERS';	

  constructor() { }

  ngOnInit() {

  	$(document).ready(function(){
  		$("body").removeClass("hold-transition").removeClass("login-page");

  		$("body").addClass("hold-transition skin-blue sidebar-mini");
 		
  	});


  }

}
