import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { first } from 'rxjs/operators';
import { ZonesService } from '../../services/zones.service';

@Component({
  selector: 'app-create-zone',
  templateUrl: './create-zone.component.html',
  styleUrls: ['./create-zone.component.css']
})
export class CreateZoneComponent implements OnInit {

	title = 'ADD ZONE';

  createZoneForm: FormGroup;
  loading = false;
  submitted = false;
  das = false;
  returnUrl: string;
  error = '';
  userListByRole: any;
  zoneList: any;
  isLoader = false;
  zoneTitle: string;
  zoneData: any;
  zoneName: string;
  zoneId: number;
  msgUpdateType: string;
  msgUpdate: string;

  districtList: Array<any>;


dropdownList = [];
selectedItems = [];
dropdownSettings = {};


  editZoneForm: FormGroup;
  addPostalForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private zonesService: ZonesService) { }

  ngOnInit() {
      this.userListByRole = [];
  		$(document).ready(function(){
	  		$("body").removeClass();

	  		$("body").addClass("hold-transition skin-blue sidebar-mini");
	 		
	  	});

      this.createZoneForm = this.formBuilder.group({
          uId: ['', Validators.required],
          zoneName: ['', Validators.required]
      });

      this.editZoneForm = this.formBuilder.group({
          zoneId: ['', Validators.required],
          zoneName: ['', Validators.required]
      });

      this.addPostalForm = this.formBuilder.group({
          district: ['', Validators.required],
          zoneName: ['', Validators.required]
      });

      this.getAllBusiness();
      
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'; 



      this.dropdownList = [
      { item_id: 1, item_text: 'Mumbai' },
      { item_id: 2, item_text: 'Bangaluru' },
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' },
      { item_id: 5, item_text: 'New Delhi' }
    ];
    this.selectedItems = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.getDistricts();


  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  get f() { return this.createZoneForm.controls; }

  get y() { return this.addPostalForm.controls; }

  get z() { return this.editZoneForm.controls; }

  set z(zone){
     this.editZoneForm.setValue({
      zoneId: zone.zoneId,
      zoneName: zone.zoneName
    });
  }

  getAllBusiness(){
    this.isLoader = true;
    this.zonesService.getUsersByRole(1)
      .subscribe(
        data => {
          this.userListByRole = data;
          this.isLoader = false;
        },
        error => {
          this.isLoader = false;
        }
        );
  }

  addAZone(){

  }

  getZonesByBusiness(){
      this.isLoader = true;
      var uId = this.f.uId.value;
      this.zonesService.getZonesByBusiness(uId)
      .subscribe(
        data => {
          this.zoneList = data;
          this.isLoader = false;
        },
        error => {
          this.isLoader = false;
        }
      );
  }

  editZone(zone: any){
    this.zoneName = zone.zoneName;
    this.z = zone;
  }

  editAZone(){
    this.isLoader = true;
    var zoneId = this.z.zoneId.value;
    var zoneName = this.z.zoneName.value;
    const data = {
            "zoneId": zoneId,
            "zoneName": zoneName
        }

    this.zonesService.updateZone(data)
    .subscribe(
      data => {
        console.log(data);
        this.isLoader = false;
        this.msgUpdateType = data.status;
        this.msgUpdate = data.message;
        this.getZonesByBusiness();
      },
      error => {
        console.log(data);
        this.isLoader = false;
      }
    );    
  }

  getPostalByZoneId(zoneId: number){
    this.zonesService.getPostalCodeListInAZone(zoneId)
    .subscribe(
      response => {
        this.isLoader = false;
        this.das = false;
        this.zoneData = response;
      },
      error => {
        this.das = false;
        this.isLoader = false;
      }
    );
  }

  readZone(zoneName: string, zoneId: number){
    
    var scrollBottom = $(window).scrollTop() + $(window).height();
  $("html, body").animate({scrollTop: scrollBottom}, 1000);    
    
  }

  viewZone(zoneTitle: string, zoneId: number){
    console.log(zoneTitle);
    console.log(zoneId);

    this.isLoader = true;
    this.das = true;
    this.zoneTitle = zoneTitle;
    this.getPostalByZoneId(zoneId);
  }

  deleteZipZoneMapping(zoneId: number, zoneZipMapId: number){
    if(!confirm("Are you sure?")) return false;
    this.isLoader = true;
    this.zonesService.deleteZipZoneMapping(zoneId,zoneZipMapId)
    .subscribe(
    response => {
      console.log(response);
      this.getPostalByZoneId(zoneId);
    },
    error => {
      console.log(error);
    }
    );
  }

  deleteAZone(zoneId: number, uid: number){
    if(!confirm("Are you sure?")) return false;
    this.isLoader = true;
    this.zonesService.deleteAZone(zoneId)
    .subscribe(
    response => {
      console.log(response);
      this.getZonesByBusiness();
    },
    error => {
      console.log(error);
    }
    );
  }

  getDistricts(){
    this.zonesService.getAllDistricts()
    .subscribe(
      response => {
        this.districtList = response;
      },
      error => {

      }
    );
  }

  getSubDistrict(district: string){
    this.zonesService.getSubDistrict(district)
    .subscribe(
      response => {

      },
      error => {

      }
    );
  }


}
