import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { first } from 'rxjs/operators';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  title = 'SETTINGS';

  settingsForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  auth: Object;
  errmsg: string;
  apis: Object;




  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private settingsService: SettingsService) { }

  ngOnInit() {

  this.apis = {data:[]};

  	$(document).ready(function(){
  		$("body").removeClass("hold-transition").removeClass("login-page");

  		$("body").addClass("hold-transition skin-blue sidebar-mini");
 		
  	});

  	this.settingsForm = this.formBuilder.group({
        keyTitle: ['', Validators.required],
        metaKey: ['', Validators.required],
        matrixapikey: ['', Validators.required]
    });

    this.getAllApis();
   

  }

  get f() { return this.settingsForm.controls; }

  set f(api){
  	 this.settingsForm.setValue({
			keyTitle: api.keyTitle,
			metaKey: api.metaKey,
			matrixapikey: api.matrixapikey
		});
  }

  saveAPI(){

  	this.submitted = true;
  	if (this.settingsForm.invalid) {
        return;
    }

    this.loading = true;
    
    if (this.f.keyTitle.value == "" || this.f.metaKey.value == "" || this.f.matrixapikey.value == "") {
            this.error = "All fields are mandatory.";
           alert("All fields are mandatory.");
            return;
        }


    this.settingsService.saveApi(this.f.keyTitle.value, this.f.metaKey.value, this.f.matrixapikey.value)
    .pipe(first())
    .subscribe(
    	response => {


    		
            this.loading = false;
            this.getAllApis();

    	},
    	error => {
    		alert("Something went wrong");
    		this.loading = false;
    	}
    );     
  }

  getAllApis(){

  	 this.settingsService.getApisList()
    .subscribe(
    	data => {
    		this.apis = data;
    		console.log(data);
    	});


  }

  setAApi(setapi){
        
  		this.f = setapi;
        $("html, body").animate({scrollTop: 0}, 1000);
    }

  deleteSetting(id){
  	if(!confirm("Are you sure?")){
            return false;
        }
  	this.loading = true;
  	this.settingsService.deleteASetting(id)
    .subscribe(
    	response => {
    		this.loading = false;
    		this.getAllApis();
    	},
    	error => {
    		this.loading = false;
    	});
  } 

  generateALzWebHookToken(){

  	if(!confirm("Are you sure?")){
            return false;
        }
  	this.loading = true;
  	this.settingsService.generateLzWebHookToken()
    .subscribe(
    	response => {
    		this.loading = false;
    		this.getAllApis();
    	},
    	error => {
    		this.loading = false;
    	});

  } 







  




}
