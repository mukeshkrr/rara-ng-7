app.controller('rabbitCtrl', function($scope, $http, $localStorage,$filter) {
    console.log("Rabbit Controller");

    $scope.searchTxt="";
    $scope.rabbitList=[];
    $scope.activePage = 0;

    function init(){
        $scope.getRabbitList(0);
        
        var catUrl = baseUrl+"/zone/zonelist";
 	   $http.get(catUrl,{ cache: true})
 		   .then(function(response){

 		   	$scope.zoneList = response.data;
 		   	console.log(response.data);
 		   }).finally(function(){

 		   });
        
        
        
    }



    var driverlist = "#driverlist";
    $scope.$watch(function() { return angular.element(driverlist).is(':visible') }, function() {
        if(angular.element(driverlist).is(':visible') != true){
            return false;
        }


        $scope.getRabbitList(0);
        
        var catUrl = baseUrl+"/zone/zonelist";
       $http.get(catUrl,{ cache: true})
           .then(function(response){

            $scope.zoneList = response.data;
            console.log(response.data);
           }).finally(function(){

           });
       
        

    });






    var scannerlist = "#scannerlist";
    $scope.$watch(function() { return angular.element(scannerlist).is(':visible') }, function() {
        if(angular.element(scannerlist).is(':visible') != true){
            return false;
        }

        $scope.scannerStatus = [{"title":"Active", "st":"Y"}, {"title":"Inactive", "st":"N"}];
       
        $scope.getScannerList(0);

    });

    $scope.saveScanner = function(scanner){


        $http({
            method: 'POST',
            url: baseUrl + "/zone/save-scanner",
            data: scanner,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
            showLoader(false);
            console.log(response);
            //var jsonObj = JSON.parse(response);
            var uid = parseInt(response.user.id);
            console.log(uid);
            if(uid > 0){
                $.sweetModal({
                    content: "Scanner added successfully",
                    icon: $.sweetModal.ICON_SUCCESS
                 });
                $scope.getScannerList(0);
            }
            
            
        }).error(function(response) {
           
            $.sweetModal({
                content: "Error! Email or mobile number already exists.",
                icon: $.sweetModal.ICON_ERROR
            });
            showLoader(false);
        });


    }
    
    $scope.mapDriverZone = function(zoneId, driverId){
    	if(!confirm("Are you sure, You want to modify zone?")){
    		return false;
    	}
    	showLoader(true);
    	var data = {zoneId:zoneId,driverId:driverId};
    	
    	console.log(data);

        $http({
            method: 'POST',
            url: baseUrl + "/rabbit/update-zone",
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
        	
        	$.sweetModal({
                content: "Driver Zone Updated",
                icon: $.sweetModal.ICON_SUCCESS
            });
            
        	showLoader(false);
            
        }).error(function(response) {
        	$.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
            console.log(response);
            showLoader(false);
        });
    		
    }

    $scope.getRabbitList = function(page) {
        showLoader(true);
console.log("Page:"+page);
        $http({
            method: 'GET',
            url: baseUrl + "/rabbit/allRabbits/"+page+"?searchTxt="+$scope.searchTxt,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.rabbitList = response;
                $scope.activePage = page;


                $scope.totalPastPages = response.rabbits.totalPages;
                $scope.totalElements = response.rabbits.totalElements;





                showLoader(false);
                
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    }

    $scope.getScannerList = function(page) {
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/rabbit/allScanner/"+page+"?searchTxt="+$scope.searchTxt,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.rabbitList = response;
                $scope.activePage = page;


                $scope.totalPastPages = response.rabbits.totalPages;
                $scope.totalElements = response.rabbits.totalElements;

                showLoader(false);
                
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    }

    $scope.getRabbitAppointment=function(rabbit){
       return (rabbit.rabbitappointmentdate?rabbit.rabbitappointmentdate:'')+' - '+(rabbit.rabbitappointmenttime?rabbit.rabbitappointmenttime:'');
    }

    $scope.getRabbitDocument = function(rabbit) {
        showLoader(true);

        $http({
            method: 'GET',
            url: baseUrl + "/rabbit/getDocument/"+rabbit.mobileno,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.rDocument = response;
                if($scope.rDocument){
                    $scope.rDocument.ktpFront=$scope.rDocument.ktpFront?baseUrl+"/rabbit/document/"+$scope.rDocument.ktpFront:'';
                    $scope.rDocument.familyCardFront=$scope.rDocument.familyCardFront?baseUrl+"/rabbit/document/"+$scope.rDocument.familyCardFront:'';
                    $scope.rDocument.drivingLicenceFront=$scope.rDocument.drivingLicenceFront?baseUrl+"/rabbit/document/"+$scope.rDocument.drivingLicenceFront:'';
                    $scope.rDocument.passportPhoto=$scope.rDocument.passportPhoto?baseUrl+"/rabbit/document/"+$scope.rDocument.passportPhoto:'';
                    $scope.rDocument.vechiclePhoto=$scope.rDocument.vechiclePhoto?baseUrl+"/rabbit/document/"+$scope.rDocument.vechiclePhoto:'';
                }
                $("#myModal").modal({backdrop: 'static', keyboard: false});
                showLoader(false);
                
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    }

    $scope.updateRabbitInfo = function(rabbit) {
        console.log(rabbit);

        $http({
            method: 'POST',
            url: baseUrl + "/rabbit/updaterabbitinfo",
            data: rabbit,
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            console.log(response);
            $.sweetModal({
                content: "Driver Information Updated",
                icon: $.sweetModal.ICON_SUCCESS
            });
            showLoader(false);
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    }

    

    $scope.getPastOrderByRabbitClicked = function(mobile){
        $scope.pastOrderRabbitId = mobile;
    }

    $scope.getPastOrderByRabbit = function(rabbitId,date,page){
       showLoader(true); 
       var catUrl = baseUrl+"/booking/rabbitPastBooking/"+rabbitId+"/"+date+"/"+page;
       $http.get(catUrl,{ cache: true})
           .then(function(response){
                $scope.pastOrderList = response.data.content;
                $scope.totalPastPages = response.data.totalPages;
                $scope.totalPastElements = response.data.totalElements;
                $scope.activePastPage = page;
            console.log(response);
           }).finally(function(){
            showLoader(false);
           });
        
    }



});

app.filter('range', function() {
	  return function(input, total) {
	    total = parseInt(total);
	    for (var i=0; i<total; i++)
	      input.push(i);
	    return input;
	  };
	});